// Mux Shield II to serial

// http://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/MuxShield.zip
#include <MuxShield.h>

MuxShield muxShield;

void setup()
{
    //Set I/O 1, I/O 2, and I/O 3 as analog inputs
    muxShield.setMode(1,ANALOG_IN);
    muxShield.setMode(2,ANALOG_IN);
    
    Serial.begin(38400);
}

//Arrays to store analog values after recieving them
byte IO1AnalogVals[10];
byte IO2AnalogVals[10];

void loop()
{
  for (int i=0; i<10; i++)
  {
    //Analog read on all 16 inputs on IO1, IO2, and IO3
    IO1AnalogVals[i] = muxShield.analogReadMS(1,i) / 8; // 0-127
    IO2AnalogVals[i] = muxShield.analogReadMS(2,i) / 8;
  }

  for (int i=0; i<10; i++)
  {
    Serial.print(i);
    Serial.print(" ");
    Serial.print(IO1AnalogVals[i]);
    Serial.println();
  }
  for (int i=0; i<10; i++)
  {
    Serial.print(10+i);
    Serial.print(" ");
    Serial.print(IO2AnalogVals[i]);
    Serial.println();
  }
  
  delay(50);
}
